//
// Created by oreushka on 01.01.2022.
//

#include "../include/image.h"
#include <stdlib.h>

struct image;

struct image constructor (const struct image* image) {
    return (struct image) {.data = malloc(sizeof(struct pixel) * image->width * image->height), .width = image->height, .height = image->width};
}

void destructor (const struct image* image) {
    free(image->data);
}
