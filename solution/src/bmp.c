//
// Created by oreushka on 23.12.2021.
//
#include "../include/bmp.h"

#include <stdio.h>
#include <stdlib.h>

#define BM 0x4D42 //'BM'
#define SIZE 40
#define BITS 24 //наш бмп 24битный

struct bmp_header;

uint64_t padding(uint64_t w) {
    return w % 4;
}

struct bmp_header header(struct image const img) {
    return (struct bmp_header){
            .bfType = BM,
            .bfileSize = (sizeof(struct bmp_header) + img.height* img.width * sizeof(struct pixel)
                          + img.height * padding(img.width)),
            .bfReserved = 0, //Зарезервирован и должен быть нулём
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = 1,
            .biBitCount = BITS,
            .biCompression = 0,
            .biSizeImage = img.height * img.width * sizeof(struct pixel) + padding(img.width)*img.height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
}

enum read_status read_error_checker (struct bmp_header* header) { //докинула проверок после код-ревью
    if (header->bfType != BM) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biSize != SIZE) {
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != BITS) {
        return READ_INVALID_BITS;
    }
    if(!(header->biWidth > 0 && header->biHeight > 0)) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status write_error(const struct image* image) {
    destructor(image);
    return WRITE_ERROR;
}


enum read_status from_bmp( FILE* in, struct image* image ) {

    struct bmp_header* header = malloc(sizeof(struct bmp_header)); //аллоцириуем память для инфы о файле

    fread(header, sizeof(struct bmp_header), 1, in); //читаем заголовок

    image->data = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight); //аллоцируем память для массива с пикселями

    image->width = header->biWidth;
    image->height = header->biHeight;

    for(uint32_t i = 0; i < header->biHeight; i++) { //будем записывать пиксели, добивая мусором
        /*size_t fread( void * ptrvoid, size_t size, size_t count, FILE * filestream );
         * ptrvoid - Указатель на блок памяти, размер которого должен быть минимум  (size*count) байт
         * size - размер в байтах каждого считываемого элемента.
         * count - Количество элементов, каждый из которых имеет размер size байт.
         * filestream - Указатель на объект типа FILE, который связан с потоком ввода.*/
        if(!(fread(&(image->data[i * image->width]), sizeof(struct pixel), header->biWidth, in))) {
            free(header);
            return READ_INVALID_HEADER;
        }

        /*int fseek( FILE * filestream, long int offset, int origin );
         * filestream - Указатель на объект типа FILE, идентифицируемый поток.
         * offset - Количество байт для смещения, относительно некоторого положения указателя.
         * origin - Позиция указателя, относительно которой будет выполняться смещение. Такая позиция задаётся одной из следующих констант, определённых в заголовочном файле <cstdio>*/
        fseek( in, padding(header->biWidth), SEEK_CUR);
    }

    enum read_status status = read_error_checker(header);

    free(header);

    return status;
}

enum write_status to_bmp( FILE* out, const struct image* image ) {
    struct bmp_header head = header(*image); //делаем заголовок для нового файла

    fwrite(&head, sizeof(struct bmp_header), 1, out); //пишем новый файл!

    const size_t padd = 0;

    const uint32_t width = image->width;
    const uint32_t height = image->height;

    for(uint32_t cur_h = 0; cur_h < height; cur_h++) {
        /*size_t fwrite( const void * ptrvoid, size_t size, size_t count, FILE * filestream );
         *ptrvoid - Указатель на массив элементов, которые необходимо записать в файл.
         * size - Размер в байтах каждого элемента массива.
         * count - Количество элементов, каждый из которых занимает size байт.
         * filestream - Указатель на объект типа FILE, который связан с потоком вывода.
        */
        if(!(fwrite(&(image->data[cur_h * width]),sizeof(struct pixel), width, out))) write_error(image);
        if(!(fwrite(&padd, 1, padding(width), out))) write_error(image); //мусорные биты
    }
    return WRITE_OK;
}
