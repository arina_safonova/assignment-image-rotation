//
// Created by oreushka on 01.01.2022.
//
#include "../include/image.h"
#include "../include/rotate.h"

struct image rotate(const struct image image ) {
    struct image output = constructor(&image);

    const uint32_t width = image.width;
    const uint32_t height = image.height;

    for (uint32_t i = 0; i < width; i++) {
        for (uint32_t j = 0; j < height; j++) {
            output.data[i * height + j] = image.data[(height - 1 - j) * width + i];
        }
    }

    destructor(&image);

    return output;
}
