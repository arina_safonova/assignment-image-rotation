#include "../include/bmp.h"
#include "../include/rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    struct image image; //входное изображение
    struct image result; //выходное изображение

    FILE *file = fopen(argv[1], "rb"); //открываем поток
    from_bmp(file, &image); //вытаскиваем бмп
    fclose(file); //закрываем поток 1

    FILE *output = fopen(argv[2], "wb"); //открываем второй поток

    result = rotate(image); //перевернули на 90 против часовой
    to_bmp(output, &result); //делаем из повернутых данных бмп

    fclose(output); //закрываем поток 2
    destructor(&result);

    return 0;
}
