//
// Created by oreushka on 01.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

struct image rotate( struct image const image );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
