//
// Created by oreushka on 23.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType; // Magic identifier: 0x4d42
    uint32_t  bfileSize; // File size in bytes
    uint32_t bfReserved; // Not used
    uint32_t bOffBits; // Offset to image data in bytes from beginning of file
    uint32_t biSize; // DIB Header size in bytes
    uint32_t biWidth; // Width of the image
    uint32_t  biHeight; // Height of image
    uint16_t  biPlanes; // Number of color planes
    uint16_t biBitCount; // Bits per pixel
    uint32_t biCompression; // Compression type
    uint32_t biSizeImage; // Image size in bytes
    uint32_t biXPelsPerMeter; // Pixels per meter xAxis
    uint32_t biYPelsPerMeter; // Pixels per meter yAxis
    uint32_t biClrUsed; // Number of colors
    uint32_t  biClrImportant; // Important colors
};
#pragma pack(pop)

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* image );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* image );
#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
