//
// Created by oreushka on 01.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image constructor (const struct image* image);

void destructor (const struct image* image);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
